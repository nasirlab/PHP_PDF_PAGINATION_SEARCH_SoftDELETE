<?php 
/**
* Class for Employee
*/

namespace App\employee;
use PDO;

class Employee extends PDO{
	private $dsn       = 'mysql:host=localhost;dbname=db_bitm';
	private $dbuser    = 'root';
	private $dbpassword='';

	private $emp_title        = '';
	private $emp_designation  = '';
	private $emp_salary       = '';
	private $id               = '';
	private $keyword          = '';
	
	function __construct(){
		try{
			 parent::__construct($this->dsn,$this->dbuser,$this->dbpassword);
			 session_start();
		}catch(PDOException $e){
			echo 'Error'.$e->getMessage();
		}
		
	}

	public function setData($data){
		if(array_key_exists('emp_title',$data)){
			$this->emp_title = $data['emp_title'];
		}
		if(array_key_exists('emp_designation',$data)){
			$this->emp_designation = $data['emp_designation'];
		}
		if(array_key_exists('emp_salary',$data)){
			$this->emp_salary = $data['emp_salary']; 
		}		
		if(array_key_exists('id',$data)){
			$this->id = $data['id']; 
		}		
		if(array_key_exists('keyword',$data)){
			$this->keyword = trim($data['keyword']); 
		}

		return $this;
	}
	//Read data
	public function index($numbOfItemPerPage="", $offsetdata=""){
		$limit = (!empty($numbOfItemPerPage))?'LIMIT':false;
		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM tbl_employee ORDER BY emp_id DESC
				 $limit $numbOfItemPerPage
			 		 	OFFSET $offsetdata ";
		$stmt =$this->prepare($sql);
		$stmt->execute();
		$empInfo = $stmt->fetchALL();
		$empInfo['totalRows']=$this->query('SELECT FOUND_ROWS()')->fetch(PDO::FETCH_COLUMN);
		
		return $empInfo;
	}	
	//Employee details 
	public function show(){
		$sql = "SELECT * FROM tbl_employee WHERE emp_id = $this->id";
		$stmt =$this->prepare($sql);
		$stmt->execute();
		$empDetails = $stmt->fetch();
		return $empDetails;
	}
	//Search data
	public function search(){
		$sql = "SELECT * FROM tbl_employee WHERE emp_title LIKE '%$this->keyword%' OR emp_designation LIKE '%$this->keyword%' ";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$searchResult = $stmt->fetchALL();
		return $searchResult;
	}
	//insert data
	public function store(){
		$sql = "INSERT INTO tbl_employee(emp_title,emp_designation,emp_salary) VALUES(:emp_title,:emp_designation,:emp_salary)";
		$stmt = $this->prepare($sql);
		$stmt->execute(array(
			':emp_title'=>$this->emp_title,
			':emp_designation'=>$this->emp_designation,
			':emp_salary'=>$this->emp_salary,
		));
		if($stmt){
			session_start();
			$_SESSION['msg'] = "Employee are added";
			header('Location:create.php');
		}else{
			$_SESSION['msg'] = "Some thing Worng !!";
			header('Location:create.php');
		}	
	}
	//Update data
	public function update(){
		$sql = "UPDATE tbl_employee SET 
					emp_title ='$this->emp_title' ,
						emp_designation ='$this->emp_designation',
							emp_salary  ='$this->emp_salary'
								WHERE emp_id=$this->id ";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		if($stmt){
			session_start();
			$_SESSION['msg'] = "Employee info  are updated";
			header('Location:index.php');
		}else{
			$_SESSION['msg'] = "Not Updated !!";
			header('Location:index.php');
		}
	}
	//Delete data 
	public function delete(){
		$sql = "DELETE FROM tbl_employee WHERE emp_id = $this->id";
		$stmt = $this->prepare($sql );
		$stmt->execute();
		if($stmt){
			$_SESSION['msg'] = "Data deleted";
			header('Location:index.php');
		}

	}


} //Employee End class