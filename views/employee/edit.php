<?php 
	include_once("../../vendor/autoload.php");
	use App\employee\Employee;
	$empObj = new Employee;
	$empDetails = $empObj->setData($_GET)->show();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Add New Employee</title>
</head>
<body>
	<section>
		<fieldset style="background: #fcfcfc; width: 500px; margin: 0 auto;">
			<legend>Add new employee</legend>
			<form action="update.php" method="POST">
					<table align="center" cellspacing="0" style="padding: 20px;">
					 	<tr>
							<th colspan="2"><a href="index.php">Employee List</a></th>
						</tr>
						<tr>
							<td><label>Name</label></td>
							<td><input type="text" value="<?php echo $empDetails['emp_title']?>" name="emp_title" autofocus></td>
						</tr>				
						<tr>
							<td><label>Designation</label></td>
							<td>
								<select name="emp_designation">
									<option><?php echo $empDetails['emp_designation']?></option>
									<option>Director</option>
									<option>Asst.Director</option>
									<option>Asst.Director</option>
									<option>GM</option>
									<option>AGM</option>
									<option>Supervisor</option>
									<option>Worker</option>
									<option>Security Officer</option>
									<option>Security Gaurd</option>
								</select>
							</td>
						</tr>							
						<tr>
							<td><label>Salary</label></td>
							<td><input type="text" value="<?php echo $empDetails['emp_salary']; ?>" placeholder="In Taka" name="emp_salary" autofocus></td>
						</tr>				
						<tr>
							<td></td>
							<td><input type="hidden" value="<?php echo $empDetails['emp_id']; ?>" name="id" ></td>
							<td><input type="submit" value="UPDATE" name="submit" ></td>
						</tr>
					</table>
			</form>
		</fieldset>
	</section>
</body>
</html>