<!DOCTYPE html>
<html>
<head>
	<title>Add New Employee</title>
</head>
<body>
	<section>
		<fieldset style="background: #fcfcfc; width: 500px; margin: 0 auto;">
			<legend>Add new employee</legend>
			<form action="store.php" method="POST">
					<table align="center" cellspacing="0" style="padding: 20px;">
					 	<tr>
							<th colspan="2"><a href="index.php">Employee List</a></th>
						</tr>
						<tr>
							<th>
								<?php session_start();
								if(isset($_SESSION['msg'])){
									echo "<h3>".$_SESSION['msg']."</h3>";
									 unset($_SESSION['msg']);
								}?>
							</th>
						</tr>
						<tr>
							<td><label>Name</label></td>
							<td><input type="text" name="emp_title" autofocus></td>
						</tr>				
						<tr>
							<td><label>Designation</label></td>
							<td>
								<select name="emp_designation">
									<option>Select any one</option>
									<option>Director</option>
									<option>Asst.Director</option>
									<option>Asst.Director</option>
									<option>GM</option>
									<option>AGM</option>
									<option>Supervisor</option>
									<option>Worker</option>
									<option>Security Officer</option>
									<option>Security Gaurd</option>
								</select>
							</td>
						</tr>							
						<tr>
							<td><label>Salary</label></td>
							<td><input type="text" placeholder="In Taka" name="emp_salary" autofocus></td>
						</tr>				
						<tr>
							<td></td>
							<td><input type="submit" value="ADD" name="submit" ></td>
						</tr>
					</table>
			</form>
		</fieldset>
	</section>
</body>
</html>