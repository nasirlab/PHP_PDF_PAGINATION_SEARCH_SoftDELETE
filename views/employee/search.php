<?php 
	include_once("../../vendor/autoload.php");
	use App\employee\Employee;
	$empObj = new Employee;
	if (isset($_GET['keyword'])) {
		$empInfo = $empObj->setData($_GET)->search();
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>List of employee</title>
</head>
<body>
	<section class="mainContent">
		<table align="center" border="1px" cellspacing="0" style="padding: 20px; background:#fcfcfc;">
			<tr>
				<th colspan="4">Empoloyee List</th>
			</tr>
			<tr>
				<?php if(isset($_SESSION['msg'])){ ?>
					<th colspan="4">
						<?php	
							echo "<h3>".$_SESSION['msg']."</h3>";
							unset($_SESSION['msg']); ?>
					</th>	 
				<?php }?>

		 	</tr>					
			<tr>
				<th><a href="create.php">Add Employee</a></th>
				<th colspan="2"><a href="pdf.php">Dowanload as pdf</a></th>
				<th><a href="#">Recycle Bin</a></th>

			</tr>
			<tr>
				<th>Sl no:</th>
				<th>Name</th>
				<th>Dedignation</th>
				<th>Action</th>
			</tr>


			<?php 
				$Slno = 1;
				foreach($empInfo as $info){ ?>
					<tr>
						<td><?php echo $Slno++; ?></td>
						<td><?php echo $info['emp_title']; ?></td>
						<td><?php echo $info['emp_designation']; ?></td>
						<td>
							<a href="show.php?id=<?php echo $info['emp_id']; ?>">Details</a> ||
							<a href="edit.php?id=<?php echo $info['emp_id']; ?>">Edit</a>||
							<a href="softDelete.php?id=<?php echo $info['emp_id']; ?>">Delete</a>
						</td>
					</tr>	
			<?php }	?>
		</table>
		
	</section>
</body>
</html>
